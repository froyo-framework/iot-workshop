# IoT Workshop

Repo ini berisi contoh kode untuk workshop IoT dengan ESP8266

Contoh yang sudah ada:

	- ESP dan LED sebagai dasar pemrograman ESP
	- ESP dan servo
	- ESP dan MQTT untuk mengendalikan LED via internet

Semua pemrograman dilakukan dengan Arduino IDE
