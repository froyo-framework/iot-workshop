#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <PubSubClient.h>
#include <DHT.h>

#define DHTTYPE DHT11
#define DHTPIN D5
#define LED_BUILTIN 2

DHT dht(DHTPIN, DHTTYPE);
ESP8266WiFiMulti WiFiMulti;

WiFiClient client;
PubSubClient mqttClient(client);

const char* mqttServer = "192.168.1.64";
// track the last connection time
unsigned long lastConnectionTime = 0; 
// post data every 10 seconds
const unsigned long postingInterval = 5L * 1000L;


void mqttpublish() {
  // Read temperature from DHT sensor
  //float t = dht.readTemperature(true);
  float t = 9.5;
  // Read humidity from DHT sensor
  float h = dht.readHumidity();
  // Create data string to send to ThingSpeak
  //String data = String("field1=" + String(t, DEC) + "&field2=" + String(h, DEC));
  String data = String(t, DEC);
  // Get the data string length
  int length = data.length();
  char msgBuffer[length];
  // Convert data string to character buffer
  data.toCharArray(msgBuffer,length+1);
  Serial.println(msgBuffer);
  // Publish data to ThingSpeak. Replace <YOUR-CHANNEL-ID> with your channel ID and <YOUR-CHANNEL-WRITEAPIKEY> with your write API key
  mqttClient.publish("kantor/pintu",msgBuffer);
  // note the last connection time
  lastConnectionTime = millis();
}

void mqttsubscribe() {
  mqttClient.subscribe("kantor/pintu");
}


void reconnect() 
{
  // Loop until we're reconnected
  while (!mqttClient.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    // Connect to the MQTT broker
    if (mqttClient.connect("ArduinoWiFi101Client")) 
    {
      Serial.println("connected");
      mqttsubscribe();
    } else 
    {
      Serial.print("failed, rc=");
      // Print to know why the connection failed
      // See http://pubsubclient.knolleary.net/api.html#state for the failure code and its reason
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying to connect again
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String mess = "";
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    //Serial.print((char)payload[i]);
    mess += (char)payload[i];
  }
  Serial.print(mess);
  Serial.println();

  if(mess == "buka") {
    digitalWrite(LED_BUILTIN, HIGH);
  } else if(mess == "tutup") {
    digitalWrite(LED_BUILTIN, LOW);
  }
}

void setup() {
  Serial.begin(9600);
  delay(10);
  
  // start by connecting to a WiFi network
  WiFiMulti.addAP("Framework", "flux12345678");

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi...");

  while(WiFiMulti.run() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);

  // Set MQTT broker details
  mqttClient.setServer(mqttServer, 1883);

  // Set Callback for message received
  mqttClient.setCallback(callback);
  
  pinMode(LED_BUILTIN, OUTPUT);
  dht.begin();

  Serial.println("DHT is started");
  
  delay(500);
}

void loop() {
  // Check if MQTT client has connected else reconnect
  if (!mqttClient.connected()) 
  {
    reconnect();
  }
  
  // Call the loop continuously to establish connection to the server
  mqttClient.loop();
  
  // If interval time has passed since the last connection, Publish data to ThingSpeak
  if (millis() - lastConnectionTime > postingInterval) 
  {
    // turn on the LED
    //digitalWrite(LED_BUILTIN, HIGH);
    //mqttpublish();
    // turn off the LED
    //digitalWrite(LED_BUILTIN, LOW);
  }
}
